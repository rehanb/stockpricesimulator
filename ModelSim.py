import math
import numpy as np
import pandas as pd

class ModelSim(object):

    def __init__(self, mu, starting_stock_price, volatility, time_step_in_num_days, num_simulations):
        self.mu = mu
        self.start_stock_price = starting_stock_price
        self.volatility = volatility
        self.time_step_in_num_days = time_step_in_num_days
        self.num_simulations = num_simulations

    def simulated_prices_gbm(self):
        df = pd.DataFrame(np.ones(self.num_simulations).transpose(), columns=['ones'])
        df['mu'] = self.mu
        df['vol'] = self.volatility
        df['time'] = self.time_step_in_num_days/365.0
        df['z'] = df['ones'].apply(lambda x: np.random.normal())
        return self.start_stock_price * (np.exp(df['mu'] - df['vol']*df['vol']*0.5*df['time'] /
                                         + df['vol']*np.sqrt(df['time'])*df['z']))